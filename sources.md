# Sources:
* https://trac.torproject.org/projects/tor/wiki/doc/ReducedExitPolicy
* https://raw.githubusercontent.com/katmagic/Tor/master/contrib/tor-exit-notice.html
* https://github.com/katmagic/Tor/blob/master/contrib/tor-exit-notice.html
* https://www.torproject.org/docs/tor-manual.html.en
* https://gist.github.com/haarts/3891752
* https://blog.torproject.org/tips-running-exit-node